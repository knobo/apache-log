#|
  This file is a part of apache-log project.
  Copyright (c) 2015 Knut Olav Bøhmer (bohmer@gmail.com)
|#

(in-package :cl-user)
(defpackage apache-log-test-asd
  (:use :cl :asdf))
(in-package :apache-log-test-asd)

(defsystem apache-log-test
  :author "Knut Olav Bøhmer"
  :license "LLGPL"
  :depends-on (:apache-log
               :prove)
  :components ((:module "t"
                :components
                ((:test-file "apache-log"))))
  :description "Test system for apache-log"

  :defsystem-depends-on (:prove-asdf)
  :perform (test-op :after (op c)
                    (funcall (intern #.(string :run-test-system) :prove-asdf) c)
                    (asdf:clear-system c)))
