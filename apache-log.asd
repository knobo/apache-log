#|
  This file is a part of apache-log project.
  Copyright (c) 2015 Knut Olav Bøhmer (bohmer@gmail.com)
|#

#|
  Author: Knut Olav Bøhmer (bohmer@gmail.com)
|#

(in-package :cl-user)
(defpackage apache-log-asd
  (:use :cl :asdf))
(in-package :apache-log-asd)

(defsystem apache-log
  :version "0.1"
  :author "Knut Olav Bøhmer"
  :license "LLGPL"
  :depends-on (:esrap
	       :local-time)
  :components ((:module "src"
                :components
                ((:file "apache-log"))))
  :description "Parse apache log files"
  :long-description
  #.(with-open-file (stream (merge-pathnames
                             #p"README.markdown"
                             (or *load-pathname* *compile-file-pathname*))
                            :if-does-not-exist nil
                            :direction :input)
      (when stream
        (let ((seq (make-array (file-length stream)
                               :element-type 'character
                               :fill-pointer t)))
          (setf (fill-pointer seq) (read-sequence seq stream))
          seq)))
  :in-order-to ((test-op (test-op apache-log-test))))
