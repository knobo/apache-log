(in-package :cl-user)

(defpackage apache-log-graph
  (:use :cl :r :apache-log))

(in-package :apache-log-graph)

(r-init)

(r:enable-rcl-syntax)

(defparameter *lines* nil)

(let ((s (with-open-file (s "/tmp/log" :direction :input)
	   (loop for line = (read-line s nil nil nil)
	      while line
	      collect line))))
  (setf *lines* s)
  nil)


(defparameter *stat*
  (loop for line in *lines*
     with collection = (make-hash-table :test 'equal)
     do (destructuring-bind (ip date (method req prot) code ref
				user-agent) (apache-log::parse 'apache-log::combined line)
	  (let ((day (local-time:format-timestring 
		      nil
		      date :format '(:year #\- 
				     :month #\-
				     :day))))
	    (incf (gethash day collection 0))))
     finally (return collection)))


(let ((dates nil)
      (values nil))
  (maphash #'(lambda (key val)
	       (push key dates)
	       (push val values)) *stat*)
  (let ((x [as.Date dates]))
    (r:with-device ("/tmp/x1" :png)
      [plot.default x values :xlab "horizontal" :ylab "vertical"
      :col "blue" :type "s"  :axes nil]
      [box]
      [title "Visits per day"])))

