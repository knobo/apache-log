(in-package :cl-user)
(defpackage apache-log
  (:use :cl :esrap))

(in-package :apache-log)

(defun not-doublequote (char)
  (not (eql #\" char)))

(defun not-integer (string)
  (when (find-if-not #'digit-char-p string)
    t))

;;; Utility rules.

(defrule whitespace (+ (or #\space #\tab #\newline))
  (:constant nil))

(defrule nothing whitespace
  (:lambda (arg)
    (declare (ignore arg))
    (values)))

(defrule alphanumeric (alphanumericp character))

(defrule char (graphic-char-p character))

(defrule alpha   (alpha-char-p character))
(defrule numeric (digit-char-p character))

(defrule string-char (or (not-doublequote character) (and #\\ #\")))


(defrule common (and ip whitespace "-" whitespace "-" whitespace 
		     date whitespace 
		     string whitespace  ;; request
		     integer whitespace ;; responce code
		     (or integer "-" )  ;; size
		     )
  (:destructure (ip _ ing _ remu _ date _ req _ code _ size)
		(declare (ignore _))
		(list  ip date req code)))


(defrule combined (and ip whitespace      ;; (%h)
		       "-" whitespace     ;; (%l) RFC 1413 identity (ident)
		       "-" whitespace     ;; (%u) REMOTE_USER
		       date whitespace    ;; (%t) [day/month/year:hour:minute:second zone]
		       request whitespace ;; (\"%r\") "GET /apache_pb.gif HTTP/1.0" 
		       integer whitespace ;; (\"%r\") status code
		       (or integer "-" ) whitespace ;(%b) size, - if nothing delivered to user
		       string whitespace  ;;  (\"%{Referer}i\") 
		       user-agent)        ;;  (\"%{User-agent}i\")
  (:destructure (ip _ _ _ remu _ date _ req _ code _ size _ referer _ browserid)
		(declare (ignore _))
		(list ip date req code referer browserid)))


#| Date format

"[day/month/year:hour:minute:second zone]
day = 2*digit
month = 3*letter
year = 4*digit
hour = 2*digit
minute = 2*digit
second = 2*digit
zone = (`+' | `-') 4*digit" |#

;; TODO Fix timezone
;; I'm expecing timezone to be the same in lisp as in the log file at the moment.

(defrule date (and #\[ day #\/ month #\/ year #\: time whitespace timezone #\])  
  (:destructure (q1 day sep1 month sep2 year sep3 (hour min sec) sep4 zone q2)
		(declare (ignore q1 q2 sep1 sep2 sep3 sep4 zone))
		(local-time:encode-timestamp  0 sec min hour day month year)))
					      
(defrule day integer
  (:lambda (day)
    day))

(defrule month (+ alpha)
  (:lambda (month)
    (1+ (position (text month)
		  (list "jan" "feb" "mar" "apr" "may" "jun" "jul" "aug" "sep" "oct" "nov" "dec")
		  :test 'equalp))))

(defrule year integer   ;; Actually 4 numeric (or digits)
  (:lambda (year)
    year))

(defrule time (+ (or integer #\:))
  (:destructure (hour sep1 min sep2 sec)
		(declare (ignore sep1 sep2))
		(list  hour min sec)))

(defrule timezone (and (or #\- #\+ ) numeric numeric numeric numeric )
  (:lambda (zone)
    (text zone)))  

(defrule string (and #\" (* string-char) #\")
  (:destructure (q1 string q2)
		(declare (ignore q1 q2))
		(text string)))

(defrule integer (+ numeric)
  (:lambda (list)
    (parse-integer (text list) :radix 10)))



;;; IP address (or hostname)

(defrule ip (+ (not whitespace))
  (:lambda (ip)
    (text ip)))

;; "GET /apache_pb.gif HTTP/1.0" 
(defrule request (and  #\"
		       (+ (not whitespace)) whitespace
		       (+ (not whitespace)) whitespace
		       (+ (not #\"))
		       #\")
  (:destructure  (ign1 method ign resource ign protocol ign2)
		 (declare (ignore ign1 ign2))
		 (list (text method) (text resource) (text protocol))))


(defrule user-agent string ;; todo
  (:lambda  (user-agent)
    user-agent))

